% The purpose of this function is to implement multiple shooting to create
% an optimal trajectory from an initial state to a goal state. The first
% figure  shows the inverted pendulum being driven to the goal states when 
% its original states are outside of the goal ROA, at 0 rad and 0 rad/s. 
% After getting the trajectory, we use polynomial fitting Matlab function
% to do the data fitting for our two states and our input. 

% This is one of the base functions which call other sub-codes 

clc;
close all;
clear all;

global N; % 20 discretized states
global Q; % lqr
global R; % lqr
global x0; % initial state
global xref; % goal state 



% parameters  of the inverted pendulum system 
m = 1; % mass
l = 0.5; % length
b = 0.1; % damping 
I = m*l^2; % moment of inertia 
g = 9.8; % gravity

% state space matrices
A = [0 1; (m*g*l)/I -b/I]; 
B = [0; 1/I];

% matrix Q and value R as specified in the paper 
Q = diag([10, 1]);
R = 20; 

% Utilize lqr command to get K and S matrices
[K,S,E] = lqr(A,B,Q,R);
rho = region_of_attraction(K, S, m, g, l, b, I);

% number of time periods considered
N = 20; 
% starting states, theta = 0, theta_dot = 0
x0 = [0; 0];
% goal states
xref = [pi; 0];

% goal state
hold on;
plot(pi,0,'r.', 'MarkerSize',35);

% initial state
hold on;
plot(0, 0, 'b.', 'MarkerSize',35)


umax = 3; % limitation to absolute value of input torque 
t_guess = 2; % initial guess for the amount of torque to drive system to goal
% get states and inputs from calling multiple_shooting algorithm
[X2, U2, tf] = multiple_shooting(umax, t_guess)
% store input torque 
U2 = [U2 0];
nTime = N + 1;

tSol = linspace(0,tf,nTime);

xSol = X2(1,:); % theta
ySol = X2(2,:); % theta_dot
uSol = U2; % torque
tSpan = [0,1.9725]; % time from 0 to optimal time 

% plot theta and theta_dot to produce drajectory 
hold on;
plot(xSol,ySol,'b--o','LineWidth', 1);

% order of our polynomial to fit a continuous curve to the data for theta,
% theta_dot and u 
nFit = 8;  
xFit = polyfit(tSol,xSol,nFit);
yFit = polyfit(tSol,ySol,nFit);
uFit = polyfit(tSol,uSol,nFit);
% plot theta, theta_dot, and u as a function of time 
figure(2); clf;
subplot(3,1,1); hold on;
plot(tSol,xSol,'k.','MarkerSize',15)
plot(tSol,polyval(xFit,tSol),'r-')
xlim(tSpan);
ylabel('x')
title('Polynomial approximation of trajectory')
subplot(3,1,2); hold on;
plot(tSol,ySol,'k.','MarkerSize',15)
plot(tSol,polyval(yFit,tSol),'r-')
xlim(tSpan);
ylabel('y')
subplot(3,1,3); hold on;
plot(tSol,uSol,'k.','MarkerSize',15)
plot(tSol,polyval(uFit,tSol),'r-')
xlim(tSpan);
ylabel('u')
xlabel('t')


