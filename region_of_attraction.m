% The purpose of this function is to find a single region of attraction for
% an inverted pendulum system via time-invariant LQR control. The goal
% states are theta = pi and theta_dot = 0.

% This code is based off of an example within the SOSOPT package:
% pcontain.m

% GOAL 1: Calculate J* and J_dot for time invariant LQR control 

function [rho] = region_of_attraction( K, S, m, g, l, b, I)

% u = -Kx
% values within K matrix
k1 = K(1);
k2 = K(2);

pvar x1_bar x2_bar;
% linearize around goal states 
x_bar = [x1_bar;x2_bar];

% f_cl Taylor expansion used for caluculating J_dot 
f = [ x2_bar;
 (g*l*m*x1_bar^5)/(120*I) - x2_bar*(b/I + k2/I) - (g*l*m*x1_bar^3)/(6*I) - (k1/I - (g*l*m)/I)*x1_bar];

g1 = 2 * x_bar'*S*f; % J_dot 
  

g2 = x_bar'*S*x_bar; % J_star
g22 = (x_bar-[pi;0])'*S*(x_bar-[pi;0]); % J_star with consideration of goal states

% generate h(x) (order 2) using lagragian multipliers 
z = monomials(x_bar,0:2);

% pcontain is used to determine our region of attraction
[gbnds,sopt] = pcontain(g1,g2, z);

rho = gbnds(1); % maximum rho

% Plot contours of maximal disk and set S1.
plotdomain2 = [-pi / 2, 3 / 2 * pi -10 10]; 
pcontour(g22,rho,plotdomain2,'r-') % plot region of attraction around 
hold on;                           % goal states
plot(pi, 0, 'b.', 'MarkerSize',35)
axis(plotdomain2)
xlabel('x1, theta') 
ylabel('x2, theta-dot')

 title(...
     'ROA for Inverted Pendulum with Linear Time-Invariant LQR Control')
end
