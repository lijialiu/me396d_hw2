clc;
close all;
clear all;

global N; 
global Q; 
global R; 
global x0; 
global xref;



% parameters  of the inverted pendulum system 
m = 1; % mass
l = 0.5; % length
b = 0.1; % damping 
I = m*l^2; % moment of inertia 
g = 9.8; % gravity

% state space matrices
A = [0 1; (m*g*l)/I -b/I]; 
B = [0; 1/I];

% matrix Q and value R as specified in the paper 
Q = diag([10, 1]);
R = 20; 

% Utilize lqr command to 
[K,S,E] = lqr(A,B,Q,R);
%rho = region_of_attraction(K, S, m, g, l, b, I);

%LQR_Tree structure
brach_structure.points = [pi;0];
LQR_tree(1) = brach_structure;

LQR_tree_index = 1;
reached = 1;
N = 0; 
hold on;
plot(pi,0,'b.', 'MarkerSize',35);


%loop to generate LQR tree branch
for branch = 1:5
    
    x01 = rand(1) * 2 * pi - 1.0 / 2.0 * pi; 
    x02 = rand(1) * 20 - 10;
    x0 = [x01; x02]

    goal_index = 0;
    reached = 1;
    while 1
    
        if reached == 1
            fprintf('###################reached');
            
            goal_index = goal_index + 1;
            
            if goal_index > size(LQR_tree, 2)
                break
            end
            
            xref = LQR_tree(goal_index).points
            x0
            
            if abs(xref(1) - x0(1)) < 0.2 && abs(xref(2) - x0(2)) < 0.2
                continue
            end
            
            N = 7 
        else
            fprintf('###################unreached');
            N = N + 5
            xref
            if N >= 25
                reached = 1;
                continue
            end
                
        end

        
        umax = 3;
        t_guess = 2;
        %%%multiple shooting method%%%%
        [X2, U2, tf] = multiple_shooting(umax, t_guess)

        if abs(X2(1, N+1) - xref(1)) > 0.001 || abs(X2(2, N+1) - xref(2)) > 0.001 
            reached = 0;
            continue
        end

        reached = 1
        
        brach_structure.points = [X2(1,1);X2(2,1)];
        LQR_tree_index = LQR_tree_index + 1
        LQR_tree(LQR_tree_index) = brach_structure;


%         hold on;
%         plot(X2(1,1), X2(2,1), 'r.', 'MarkerSize',35)
        
        U2 = [U2 0];
        nTime = N + 1;

        tSol = linspace(0,tf,nTime);

        xSol = X2(1,:);
        ySol = X2(2,:);
        uSol = U2;
        tSpan = [0,1.9725];

        nFit = 5;
        
        if N >= 15
            nFit = 8;
        end

        xFit = polyfit(tSol,xSol,nFit);
        yFit = polyfit(tSol,ySol,nFit);
        uFit = polyfit(tSol,uSol,nFit);

        Q = diag([10, 1])  % state cost 
        R = 20;       % input cost   
        F = [ 230.8806   49.2538;
           49.2538   10.6640];  % Terminal state cost Qf
        tol = 1e-6;  % error toleration

        linSys = @(t)getLinTraj(t,xFit,yFit,uFit,m,g,l,I,b);
        Soln = trajectoryLqr(tSol,linSys,Q,R,F,tol);
        nSoln = length(tSol);

        %get the rho of each shooting state
        for idx= 1: nSoln
            idx;
            temp_A = Soln(idx).A
            temp_B = Soln(idx).B
            temp_S = Soln(idx).S
            temp_S_dot = Soln(idx).S_dot
            temp_K = Soln(idx).K
            x1_goal = polyval(xFit,tSol(idx))
            x2_goal = polyval(yFit,tSol(idx))
            u_goal = polyval(uFit,tSol(idx))
            rho(idx) = region_of_attraction1(temp_A, temp_B, temp_K, temp_S, temp_S_dot, m, g, l, b, I, u_goal, x1_goal, x2_goal)
        end
        
        %rho limitaion
        prev_rho = rho(nSoln);
        for idx= 1:nSoln
            i = nSoln-idx+1;
            temp_S = Soln(i).S;
            x1_goal = polyval(xFit,tSol(i));            
            x2_goal = polyval(yFit,tSol(i));
         
            temp_rho(i) = prev_rho;
            if rho(i) <= prev_rho
                temp_rho(i) = rho(i);
            end
            region_of_attraction2(temp_S, x1_goal, x2_goal, temp_rho(i));
            prev_rho = temp_rho(i);
        end

        for i = 1: nSoln-1
            t_delta = tSol(i+1) - tSol(i);
            beta(i) =  (temp_rho(i + 1) - temp_rho(i)) / t_delta;
            alpha(i) = temp_rho(i) - beta(i) * tSol(i);
        end


        %%%run 10 sub region of attraction with each time invertal
        numSub = 10;

        for loop = 1 : nSoln - 1

        F_sub =  Soln(loop+1).S
        tSol_sub = linspace(tSol(loop),tSol(loop+1),numSub);

        linSys_sub = @(t)getLinTraj(t,xFit,yFit,uFit,m,g,l,I,b);
        Soln_sub = trajectoryLqr(tSol_sub,linSys_sub,Q,R,F_sub,tol);
        nSoln_sub = length(tSol_sub);

        x1_goal_set = [];
        x2_goal_set = [];
        for idx= 1: nSoln_sub

            rho_sub = beta(loop) * tSol_sub(idx) + alpha(loop);

            temp_S = Soln_sub(idx).S;
            x1_goal = polyval(xFit,tSol_sub(idx));
            x1_goal_set = [x1_goal_set x1_goal]
            x2_goal = polyval(yFit,tSol_sub(idx));  
            x2_goal_set = [x2_goal_set x2_goal]
            [sum.sample sum.h] = region_of_attraction2(temp_S, x1_goal, x2_goal, rho_sub);
            tube(idx, 1) = sum;
            tube(idx).sample; 
            
            if isempty(tube(idx).sample)
                continue
            end
            
            if tube(idx).sample(2,1)+1 == size(tube(idx).sample,2)
            
                hold on;
                h = patch(tube(idx).sample(1,2:end)', tube(idx).sample(2,2:end)', ones(size(tube(idx).sample, 2) - 1,1) * tSol_sub(idx), 'FaceColor',[0.678431, 0.847059, 0.901961]);
              
            end
        end
        end
        hp = findobj(gcf,'type','patch');
        set(hp,'edgealpha',0.2)
        view(2);
        hold on;
        plot(x1_goal_set,x2_goal_set,'b-','LineWidth',1);
        

    end

end

