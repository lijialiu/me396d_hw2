% The purpose of this file is to create the curve corresponding to rho as a
% function of time. Specifically, these results are utilized to create a
% robust tube along a given trajectory.

% This is the base code that makes reference to a number of subfunctions 


clear; clc;

m = 1; % mass
l = 0.5; % length
b = 0.1; % damping 
I = m*l^2; % moment of inertia 
g = 9.8; % gravity

nTime = 21; % discretize time into 20 sections 

% X2 contains our states theta and theta_dot along a trajectory 
X2 =[0 -0.0565 -0.2126 -0.4365 -0.6853 -0.8872 -0.9633  -0.8557 -0.5269 -0.0068 0.6070 1.2081 1.7302 2.1559 2.4863 2.7259 2.8921 3.0095 3.0884 3.1299 3.1416;...
     0 -1.1192 -1.9919 -2.4763 -2.5024 -1.5519  0.0136  2.1328  4.4387  5.9394  6.3198 5.7553 4.8230 3.8672 2.9096 2.0194 1.4043 1.0147 0.6124 0.2430  0];

 % The values of our input torque are stored in U2
U2 = [-2.9874 -2.9920   -2.9955   -2.9172   -1.2850   -0.0610    1.5648    2.9988    2.9959    2.9976...
        2.9966    2.9926    2.5285    1.4487    0.4529    0.1850    0.0453   -0.5021   -0.7502   -0.5849 0];

% time considered 
tSol = linspace(0,1.9725,nTime);

xSol = X2(1,:); % theta
ySol = X2(2,:); % theta_dot 
uSol = U2;% input torque values
% considering the optimal time of 1.9725 s
tSpan = [0,1.9725];

% order of the polynomial we used to fit the data 
nFit = 8;  

% polynomial curve fitting 
xFit = polyfit(tSol,xSol,nFit);
yFit = polyfit(tSol,ySol,nFit);
uFit = polyfit(tSol,uSol,nFit);


Q = diag([10, 1])  % Q matrix as provided in the paper  
R = 20;       % R as defined in the paper
F = [ 230.8806   49.2538;
   49.2538   10.6640];  % final Q value Q_f, came from calling lqr 
tol = 1e-6;  % when we use ODE45, this is the error tolerance 

% information for the for loop below
linSys = @(t)getLinTraj(t,xFit,yFit,uFit,m,g,l,I,b);
Soln = trajectoryLqr(tSol,linSys,Q,R,F,tol);
nSoln = length(tSol);

% for loop is used to draw regions of attraction around our 20 discretrized
% points along the trajectory
% We are computing rho's without any constraint
for idx= 1: nSoln
    idx;
    temp_A = Soln(idx).A;
    temp_B = Soln(idx).B;
    temp_S = Soln(idx).S;
    temp_S_dot = Soln(idx).S_dot;
    temp_K = Soln(idx).K;
    x1_goal = polyval(xFit,tSol(idx));
    x2_goal = polyval(yFit,tSol(idx));
    u_goal = polyval(uFit,tSol(idx));
    rho(idx) = region_of_attraction1(temp_A, temp_B, temp_K, temp_S, temp_S_dot, m, g, l, b, I, u_goal, x1_goal, x2_goal)
end

% this for loop is to place the constraint that the previous rho is less
% than or equal to the next rho as we move along the trajectory from the
% starting point to the goal state
prev_rho = rho(nSoln);
for idx= 1:nSoln
    i = nSoln-idx+1;
    temp_S = Soln(i).S;
    x1_goal = polyval(xFit,tSol(i));
    x2_goal = polyval(yFit,tSol(i));
    
    temp_rho(i) = prev_rho;
    
    if rho(i) <= prev_rho
        temp_rho(i) = rho(i);
    end
    prev_rho = temp_rho(i);
end

% compiling beta and alpha values to create function. These are the 
% coefficents of the function y = kx+b --> rho = beta*t+alpha 
for i = 1: nSoln-1
    t_delta = tSol(i+1) - tSol(i);
    beta(i) =  (temp_rho(i + 1) - temp_rho(i)) / t_delta;
    alpha(i) = temp_rho(i) - beta(i) * tSol(i);
end

% plot our continuous rho function
for i = 1 : nSoln - 1
    t_duration = linspace(tSol(i),tSol(i+1),20);
    rho_section = beta(i) * t_duration + alpha(i);
    plot(t_duration, rho_section, 'LineWidth',3)
    hold on;
    
end

xlabel('time (s)', 'FontSize',15) 
ylabel('\rho', 'FontSize',15)
title('Optimation for \rho(t) : interpolation between discretized \rho(t)', 'FontSize',15)
