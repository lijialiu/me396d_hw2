function [A, B] = getLinTraj(t,xFit,yFit,uFit, m,g,l,I,b)
% This function generates a continuous trajectory function based on points
% provided by xFit, yFit, uFit at particular times. 
% This code is based on guidance provided at:
% https://www.mathworks.com/matlabcentral/fileexchange/54432-continuous-time--finite-horizon-lqr

% create continuous funtions of time for:
x = polyval(xFit,t); % theta
y = polyval(yFit,t); % theta_dot
u = polyval(uFit,t); % input torque 

% Output A and B are used to generate the S matrix
[A,B] = getLinSys(x,y,u,m,g,l,I,b);
end