% The purpose of this function is to determine the optimal states, inputs,
% and time to drive the inverted pendulum from an initial state to the goal
% state via multiple shooting

% This code is based from the following reference:
% https://www.mathworks.com/matlabcentral/fileexchange/27351-multiple-shooting

function [X2, U2, tf] = multiple_shooting(umax, t_guess)


global R; % lqr
global Q; % lqr
global N; % 20 discretized points
global input; % used for subsequent computations 
global x0; % initial state

% number of states 
Nx = size(Q,1);
% number of inputs 
Nu = size(R,1);
Nz = Nx+Nu;

% lower and upper bounds for time, states, and input
LB_MS = -inf*ones(Nz,N);
UB_MS = inf*ones(Nz,N);
LB_MS(end,:) = -umax*ones(1,N);
UB_MS(end,:) = umax*ones(1,N);

% define lower and upper bounds 
LB_MS = [0; LB_MS(:); -inf*ones(Nx,1)];
UB_MS = [inf; UB_MS(:); inf*ones(Nx,1)];

% variables used for multiple shooting
Z_MS = repmat([x0; 0],1, N);
Z_MS = [t_guess; Z_MS(:); x0];


input.Ts = Z_MS(1) / N;


% solve multiple shooting problem using fmincon
sol_multiple = fmincon(@cost_multiple,Z_MS,[],[],[],[],LB_MS,UB_MS,@constr_multiple);

% store results 
Z_MS = sol_multiple;

tf = Z_MS(1);
X2 = zeros(Nx,N+1);
U2 = zeros(Nu,N);

% compilation of optimal states and inputs  
for k = 1:N
   X2(:,k) = Z_MS(1 + (k-1)*Nz+1:1+(k-1)*Nz+Nx);
   U2(:,k) = Z_MS(1 + (k-1)*Nz+Nx+1:1+k*Nz);
end
X2(:,N+1) = Z_MS(1+ N*Nz+1:end);

