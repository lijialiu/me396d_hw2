% The purpose of this function is to plot regions of attraction along a
% trajectory.

% The outputs C and h are used to plot the region of attraction around the
% goal states
function [C, h] = region_of_attraction2( S, x1_goal, x2_goal, rho)

% define the goal states, theta and theta_dot
x_goal =[x1_goal; x2_goal];
pvar x1_bar x2_bar;
% linearization around goal states
x_bar = [x1_bar;x2_bar];

% J*
g22 = (x_bar - x_goal)'*S*(x_bar - x_goal);

plotdomain2 = [-pi / 2, 3 / 2 * pi -10 10]; 
[C, h] = pcontour(g22,rho,plotdomain2,'r-'); % used to plot region of attraction around 
hold on;                                     % goal states
plot(x1_goal, x2_goal, 'b.', 'MarkerSize',20) % plot region of attraction
% formatting plot 
axis(plotdomain2)
xlabel('x1, theta') 
ylabel('x2, theta-dot')
title(...
    'ROA for Inverted Pendulum with Linear Time-variant LQR Control')
end
